#!/bin/bash
# Script to create a list of installed flatpaks

dotfiles=$HOME/.dotfiles/

if [ -d $dotfiles ] 
then
    /usr/bin/flatpak list --columns=application --app > $dotfiles/flatpaks.txt
else
    echo "Error: Directory $($dotfiles) does not exists."
fi


