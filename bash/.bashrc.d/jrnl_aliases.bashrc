alias journal-edit-last='jrnl -n 1 --edit'

function journal-today() {
    arg=$1
    jrnl today: $arg
    jrnl -n 1 --edit
}